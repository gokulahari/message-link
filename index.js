require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.YOUR_PORT || process.env.port || 5000
const path = require('path')
//var cookieParser = require('cookie-parser');
var session = require('express-session');
var bodyParser = require('body-parser');
const multer = require('multer');
var fs = require('fs');

//Set up default mongoose connection
const mongoose = require('mongoose');
var mongoUrl = process.env.MONGO_URI;
mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });

//Get the default connection
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// set the view engine to ejs



app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(express.static(path.join(__dirname, 'public')));
app.use('/profile',express.static('./different/images'))

app.use(session({
    secret: "anand1234",
    resave: false,
    saveUninitialized: false
}))

app.use( bodyParser.json({limit: '50mb'}) );
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit:500000
}));

const initRouter = require('./routes/init')
app.use('/',initRouter)


const loginRouter = require('./routes/login')
app.use('/login',loginRouter)

const signUpRouter = require('./routes/signUp')
app.use('/signUp',signUpRouter)


const homeRouter = require('./routes/home')
app.use('/home',homeRouter)

const addProductRouter = require('./routes/addProduct')
app.use('/addProduct',addProductRouter)

const aboutRouter = require('./routes/about')
app.use('/about',aboutRouter)


const logoutRouter = require('./routes/logout')
app.use('/logout',logoutRouter)




app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
});
app.listen(process.env.PORT, () => {
    console.log(`Example app listening at ${port}`)
})
