
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var  det = new Schema({
    image: { type: String, required: true },
    name: { type: String, required: true },
   gender:  { type: String, required: true },
   email: { type: String, required: true },
   dateofbirth: { type: String, required: true },
   mobile: { type: String, required: true }
});

module.exports= mongoose.model('userModel', det);