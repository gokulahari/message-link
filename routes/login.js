const express = require('express')
const router = express.Router()
var Login = require('../models/loginModel');



router.get('/', notAuthenticate, (req, res) => {
    console.log('login varuthu da.already done........')
    res.render('pages/login');
})

router.post('/', (req, res) => {

    Login.findOne({ userEmail: req.body.email, password: req.body.psw }, function (err, docs) {
        if (err) {
            console.log(err)
            res.redirect('/');
        }
        else {
            req.session.email = docs.userEmail;
            req.session.password = docs.password;
            res.redirect('/home');
            res.end("login successfull");
        }
    });
})


function notAuthenticate(req, res, next) {
    if (req.session && req.session.email) {
        res.redirect('/home');
    }
    else {
        console.log("=====================after logout==================");
        return next();
    }
}
 
 
 module.exports = router