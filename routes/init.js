const express = require('express')
const router = express.Router()


router.get('/', authenticate, (req, res) => {
   res.redirect('/home');
})

function authenticate(req, res, next) {
   if (req.session && req.session.email) {
      return next();
   }
   else {
      console.log("session data in else home get", req.session);
      res.redirect('/login');
   }
}


module.exports = router