const express = require('express')
const router = express.Router()
var Login = require('../models/loginModel');





router.get('/', notAuthenticate, (req, res) => {
    res.render('pages/signUp');
})

router.post('/', (req, res) => {

    var item = {
        userEmail: req.body.email,
        password: req.body.psw,
    }

    var credentials = new Login(item);
    credentials.save();
    res.redirect('/login');
    
})




function notAuthenticate(req, res, next) {
    if (req.session && req.session.email) {
        res.redirect('/home');
    }
    else {
        console.log("=====================after logout==================");
        return next();
    }
}
module.exports = router