
const express = require('express')
const router = express.Router()
var url = require('url') ;
var UrlOrMsg = require('../models/productModel');

const loginRouter = require('./login')




router.get('/', authenticate, (req, res) => {
    UrlOrMsg.find().exec(function (err, product) {
        if (err) {
            console.log("error occured in fetching data from database");
        }
        else {
            res.render('pages/home', { name: req.session.email, products: product });
        }
    })
})

router.post('/', (req, res) => {

    url = req.body.urlOrMsg === 'message' ? `m/${random()}` : random()
    timeSplit = req.body.timer.split(' ')
    let min;
    if (timeSplit[1] === 'hour'){
        min = Number(timeSplit[0]) * 60 
    }
    else if(timeSplit[1] === 'week'){
        min = Number(timeSplit[0]) * 7 * 24 * 60
    }
    else if(timeSplit[1] === 'day'){
        min = Number(timeSplit[0]) * 24 * 60
    }
    else{
        min = Number(timeSplit[0])
    }

    console.log('url got...',min)

    var item = {
        urlOrMsgType: req.body.urlOrMsg,
        urlOrMsg: req.body.msg,
        url: url,
        timer: String(min),
        timestamp: new Date(),
    }

    console.log("data of hme", req.body);

    var urlDetails = new UrlOrMsg(item);
    urlDetails.save();
    res.render('pages/about', { url: urlDetails.url, name: req.session.email })
})

function random() {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < 8; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


function authenticate(req, res, next) {
    if (req.session && req.session.email) {
        return next();
    }
    else {
        res.redirect('/login');
    }
}

module.exports = router