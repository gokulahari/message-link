const express = require('express')
const router = express.Router()
const multer = require('multer');
const path = require('path')
var UrlOrMsg = require('../models/productModel');

router.get('/:userUrl', (req, res) => {
    console.log("addprourl", req.params)
    const param = `${req.params.userUrl}`
    const currTime = new Date()
    UrlOrMsg.find({ "url": param}).select().exec(function (err, product) {
        if (err) {
            console.log("error occured in fetching data from database");
        }
        else {
            console.log("fetched url detail", product);
            console.log("fetched url detail url", product[0].urlOrMsg);
            const oldtime = new Date(product[0].timestamp)
            let diff = (currTime.getTime() - oldtime.getTime()) / 1000
            diff /= 60;
            const result =  Math.abs(Math.round(diff))
            if(result <= product[0].timer){
                res.redirect(`https://${product[0].urlOrMsg}/`)
            }
            else{
                res.send("Page not found....");
            }
        }
    })
})

router.get('/m/:userUrl', (req, res) => {
    console.log("addpromessage", req.params)
    const param = `m/${req.params.userUrl}`
    const currTime = new Date()
    UrlOrMsg.find({ "url": param}).select().exec(function (err, product) {
        if (err) {
            console.log("error occured in fetching data from database");
        }
        else {
            console.log("fetched url detail", product);
            const oldtime = new Date(product[0].timestamp)
            let diff = (currTime.getTime() - oldtime.getTime()) / 1000
            diff /= 60;
            const result =  Math.abs(Math.round(diff))
            console.log('old timestamp', Math.abs(Math.round(diff)))
            if(result <= product[0].timer){
                res.render('pages/addProduct', { name: req.session.email, products: product[0] });
            }
            else{
                res.send("Page not found....");
            }
        }
    })
})

module.exports = router